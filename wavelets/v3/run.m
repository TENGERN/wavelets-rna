%% List all files on a given folder
% Observation: On the find_files function there is a condition that filters
% files by name.
%% Configuration
clc; clear all; close all;

%% Overwrite output file
% if exist(output_file_name, 'file')==2
%   delete(output_file_name);
% end
%% Start
% generate file list
find_files('../../dataset/tidigits20/train/woman/', 'list_tidigits20_woman_3lvl.txt');
find_files('../../dataset/tidigits20/train/man/', 'list_tidigits20_man_3lvl.txt');
find_files('../../dataset/tidigits8/train/man/', 'list_tidigits8_man_3lvl.txt');
find_files('../../dataset/tidigits8/train/woman/', 'list_tidigits8_woman_3lvl.txt');

% wavelet transform
transform_signal('list_tidigits20_woman_3lvl.txt', 'tidigits20_woman_3lvl.txt');
transform_signal('list_tidigits20_man_3lvl.txt', 'tidigits20_man_3lvl.txt');
transform_signal('list_tidigits8_man_3lvl.txt', 'tidigits8_man_3lvl.txt');
transform_signal('list_tidigits8_woman_3lvl.txt', 'tidigits8_woman_3lvl.txt');




