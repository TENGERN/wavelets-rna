function find_files(addr, output_file_name)

%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
current_dir = dir(strcat(addr,'*'));
current_dir = current_dir(~ismember({current_dir.name},{'.','..'}));

for i=1:1:length(current_dir)
    if current_dir(i).isdir ==  1
        % not a file, enter directory
        find_files(strcat(addr,current_dir(i).name,'/'), output_file_name);
    else
        % it is a file
        if sum(strcmp(current_dir(i).name, {'1a.wav', '2a.wav', '3a.wav', '4a.wav', '5a.wav', '6a.wav', '7a.wav', '8a.wav', '9a.wav', 'za.wav'})) == 1 %'za.wav', 
            % met conditions
            fid = fopen(output_file_name, 'a');
            fprintf(fid, strcat(addr,current_dir(i).name,'\n'));
            fclose(fid);
        else
            % does not met conditions
            % nothing to do
        end
    end
end

end

