% Function dwt_trans()
%
% This function returns the transpose vector of a high resolution wavelet 
% discrete transform applied on the signal

%---------------------dwt_trans.m-------------------

function wavelet_discrete_vector = dwt_trans(x)

%-----1º Nível -------------------------------------
[A1,D1] = dwt(x,'haar');
%-----2º Nível -------------------------------------
[A2,D2] = dwt(A1,'haar');
%-----3º Nível -------------------------------------
[A3,D3] = dwt(A2,'haar');
%-----4º Nível -------------------------------------
[A4,D4] = dwt(A3,'haar');
%-----5º Nível -------------------------------------
[A5,D5] = dwt(A4,'haar');
%-----6º Nível -------------------------------------
[A6,D6] = dwt(A5,'haar');
%-----7º Nível -------------------------------------

wavelet_discrete_vector = A3';




