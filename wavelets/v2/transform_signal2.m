%% Transformada do Sinal para Reconhecimento pela RNA
% Desenvolvido por Mathias Silva da Rosa (mathiassilva4@gmail.com )
% O objetivo desta simula��o � utilizar a transformada de wavelet para
% compactar dados de voz.
clc; clear all; close all;

%% Nova listagem de arquivos
fileID = fopen('list_files.txt','r');
files = textscan(fileID,'%s');
files = files{1};
fclose(fileID);

% adapatacao das variaveis
quant_arq = length(files);
arq_matriz = files;



%%

vector_num = (quant_arq);
vector_sinais = zeros(quant_arq,1000);
vector_tam = (quant_arq);

% Gera um vetor randomizado com os valores de 1 quant_arq
% para n�o viciar a RNA posteriormente
rand_vetor = randperm(quant_arq(1));

for i=1:1:quant_arq 

   % selecionando um dos arquivos WAV aleat�riamente
   info = arq_matriz(rand_vetor(i));
   nome = info{1};
   if nome(end-5) == 'z'
       vector_num(i) = 0;
   else
       vector_num(i) = str2num(nome(end-5));
   end
   
   
   % Amostragem do sinal (no arquivo WAV) para um vetor
   [x, fa] = audioread(nome);
   
   % Retirar a partes de sil�ncio no come�o e no final
   envelope = imdilate(abs(x), true(1501, 1));
   silencio = envelope < 0.005; % Or whatever value you want.
   m = x; 
   m(silencio) = [];
   
   % Normaliza��o do vetor
   m = m/norm(m,inf);
   
   % Aplica o banco de transformadas wavelet no sinal
   y = dwt_trans(m);
   vector_sinais(i,1:length(y)) = y;
   
   % Obtendo o tamanho do m�ximo vetor 
   tam_y = size(y);
   vector_tam(i) = tam_y(2);
   
end

% Cria vetor de zeros com o m�ximo tamanho de vetor
tam_max = max(vector_tam);


%% Cria arquivo de resultados

% Matriz de convers�o para valores de entrada a RNA
array_result = [ 0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7
                -0.7 0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7
                -0.7 -0.7 0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7
                -0.7 -0.7 -0.7 0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7
                -0.7 -0.7 -0.7 -0.7 0.7 -0.7 -0.7 -0.7 -0.7 -0.7
                -0.7 -0.7 -0.7 -0.7 -0.7 0.7 -0.7 -0.7 -0.7 -0.7
                -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 0.7 -0.7 -0.7 -0.7
                -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 0.7 -0.7 -0.7
                -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 0.7 -0.7
                -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 -0.7 0.7];

for i=1:1:quant_arq
    
   r = vector_sinais(i,:); 
    
   % Concatena as amostras com zero
   Y = zeros(1,tam_max);
   Y(1:tam_max) = r(1:tam_max);
   
   % Cria��o do arquivo de treinamento da RNA
   fid = fopen('result.txt','a+');
   fprintf(fid,'%f ',Y);
   fprintf(fid,'\n');
   
   num = vector_num(i);
   
   % Escrita no arquivo conforme n�mero falado
   switch num
       case 0
          fprintf(fid,'%f ', array_result(1,:));
       case 1
          fprintf(fid,'%f ', array_result(2,:));
       case 2
          fprintf(fid,'%f ', array_result(3,:));
       case 3
          fprintf(fid,'%f ', array_result(4,:));
       case 4
          fprintf(fid,'%f ', array_result(5,:));
       case 5
          fprintf(fid,'%f ', array_result(6,:));
       case 6
          fprintf(fid,'%f ', array_result(7,:));
       case 7
          fprintf(fid,'%f ', array_result(8,:));
       case 8
          fprintf(fid,'%f ', array_result(9,:));
       case 9
          fprintf(fid,'%f ', array_result(10,:));
       otherwise
          warning('Unexpected number')
   end
   fprintf(fid,'\n');
   fclose(fid);
end 

