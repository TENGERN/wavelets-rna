%% List all files on a given folder
% Observation: On the find_files function there is a condition that filters
% files by name.
%% Configuration
clc; clear all; close all;
output_file_name = 'list_files.txt';
folder = '../../dataset-vozes-20kHz/tidigits20/';

%% Overwrite output file
if exist(output_file_name, 'file')==2
  delete(output_file_name);
end
%% Start
% Total file found = 25096
% 21450 ends with "a.wav"
% 3260 files met the condition
find_files(folder);

%% Test Read from file
% fileID = fopen('list_files.txt','r');
% formatSpec = '%s';
% files = textscan(fileID,formatSpec);
% files = files{1};
% fclose(fileID);
