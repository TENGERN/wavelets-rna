# README #

Generate wavelet coefficients for a given amount of audio files and trains a RNA to recognize them.

## Wavelets ##

### v1 ###

* Developed by Mathias Silva da Rosa (mathiassilva4@gmail.com)
* Move woman voice dataset to wavelets-rna/dataset-vozes-20kHz/woman
* Run wavelets-rna/wavelets/v1/tranform_signal.m with Matlab
* Results will be available on wavelets-rna/wavelets/v1/result.txt

### v2 ###

* Modified version by Ernani Rodrigues de São Thiago (ernanirst@gmail.com)
* Move full voice dataset to wavelets-rna/dataset-vozes-20kHz/tidigits20
* Run wavelets-rna/wavelets/v2/generate_list_files.m
* List of available files will be in wavelets-rna/wavelets/v2/list_files.txt
* Run wavelets-rna/wavelets/v2/tranform_signal2.m with Matlab
* Results will be available on wavelets-rna/wavelets/v1/result.txt

### RNA ###

* Move one of result.txt file generated with wavelet v1 or v2 to wavelets-rna/rna/result.txt
* Run wavelets-rna/rna/ex1.m with Matlab
* Results will be available on Matlab workspace as "inputs" and targets
* Now run the nprtool on Matlab to train the RNA

## Who do I talk to? ##

* Current responsible student: Ernani Rodrigues de São Thiago (ernanirst@gmail.com)
* Project Coordinators: Ramon Mayor Martins (ramon.mayor@ifsc.edu.br) and Elen Lobato (elen@ifsc.edu.br)
* Older contributors: Mathias Silva da Rosa (mathiassilva4@gmail.com)