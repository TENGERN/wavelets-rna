clc; clear all; close all;

%% Import data from file
% Import all data from the file as a 2880x305 matrix, each odd line has a
% data and the next line has it's target
filename = 'result_matias.txt';
delimiterIn = ' ';
headerlinesIn = 0;
data = importdata(filename,delimiterIn,headerlinesIn);

%% Adjust data
% Convert all odd lines to an input matrix, where each column represents an
% input, also convert the pair lines to a target matrix, where it column is
% a target.
inputs = transpose(data(1:2:end,:));
targets = transpose(data(2:2:end,:));
targets = targets(1:1:10,:);

%% New Targets
% Get a number from 0 to 9 based on the position of 0.7 on the column
%[row,col] = find(targets == 0.7);
%targets2 = row -1;
targets( targets==0.7 )=1; 
targets( targets==-0.7 )=0; 
targets2 = transpose(targets);
%% Test AND
INPUT_AND = [ 0 0 1 1; 0 1 0 1];
TARGET_AND = [0 0 0 1];
